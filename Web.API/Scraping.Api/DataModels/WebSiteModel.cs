﻿namespace Scraping.Api.DataModels
{
    using System.ComponentModel.DataAnnotations;

    using Infrastructure;
    using Model;

    public class WebSiteModel : IMapFrom<Site>
    {
        [Required]
        public string Url { get; set; }
    }
}