﻿namespace Scraping.Api.DataModels
{
    using Scraping.Api.Infrastructure;
    using Scraping.Model;

    public class ProductResponseModel : IMapFrom<Product>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ShortDescription { get; set; }

        public decimal RegularPrice { get; set; }

        public decimal SalePrice { get; set; }
    }
}