﻿namespace Scraping.Api.DataModels
{
    using Scraping.Api.Infrastructure;
    using Scraping.Model;

    public class AddSiteRequestModel : IMapFrom<Xpath>
    {
        public ProductModel Product { get; set; }

        public UserRequestModel User { get; set; }
    }
}