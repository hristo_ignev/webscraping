﻿namespace Scraping.Api.DataModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class AllProductsRequestModel
    {
        [Required]
        public UserRequestModel User { get; set; }

        public ProductRequestModel Product { get; set; }
    }
}