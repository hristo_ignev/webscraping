﻿namespace Scraping.Api.DataModels
{
    using System.ComponentModel.DataAnnotations;
    using Infrastructure;
    using Model;

    public class ProductModel : IMapFrom<Xpath>
    {
        [Required]
        public string Url { get; set; }

        [Required]
        public string NameXpath { get; set; }
        
        [Required]
        public string DescriptionXpath { get; set; }

        [Required]
        public string ShortDescriptionXpath { get; set; }
        
        public string RegularPriceXpath { get; set; }
        
        public string SalePriceXpath { get; set; }
    }
}