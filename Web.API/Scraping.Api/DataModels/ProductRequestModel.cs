﻿namespace Scraping.Api.DataModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;

    public class ProductRequestModel
    {
        [Required]
        public int SiteId { get; set; }

        [Required]
        public ICollection<string> Urls { get; set; }
    }
}