﻿namespace Scraping.Api.DataModels
{
    using System.ComponentModel.DataAnnotations;

    using Infrastructure;
    using Model;

    public class UserRequestModel : IMapFrom<User>
    {
        [Required]
        public string Name { get; set; }
    }
}