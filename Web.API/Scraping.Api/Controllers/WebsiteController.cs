﻿namespace Scraping.Api.Controllers
{
    using System.Linq;
    using System.Web.Http;

    using AutoMapper;
    using Data;
    using DataModels;
    using Model;

    public class WebsiteController : ApiController
    {
        private IScrapingData db;

        public WebsiteController(IScrapingData data)
        {
            this.db = data;
        }

        [HttpPost]
        public IHttpActionResult AddWebsite(AddSiteRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            var product = model.Product;            

            var user = this.db.Users
                .Include(s => s.Sites)
                .Where(u => u.Name == model.User.Name)
                .FirstOrDefault();

            if (user == null)
            {
                user = Mapper.Map<User>(model.User);

                this.db.Users.Add(user);
            }

            var xpath = Mapper.Map<Xpath>(product);

            var site = user
                .Sites
                .Where(s => s.Url == product.Url)
                .FirstOrDefault();

            if (site == null)
            {
                site = new Site
                {
                    Url = product.Url,
                    Xpath = xpath
                };

                user.Sites.Add(site);
            }
            else
            {
                this.db.Xpaths.Delete(site.Xpath);
                site.Xpath = xpath;
            }

            this.db.Users.SaveChanges();

            return this.Ok("Added");
        }

        [HttpGet]
        public IHttpActionResult GetAllSites(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return this.BadRequest(this.ModelState);
            }

            var user =
                this.db.Users.Include(s => s.Sites)
                .Where(u => u.Name == id).FirstOrDefault();

            if (user == null)
            {
                return this.NotFound();
            }

            var getWebsites = user.Sites
                .Select(u => new { Url = u.Url, Id = u.Id })
                .ToList();

            return this.Ok(getWebsites);
        }
    }
}
