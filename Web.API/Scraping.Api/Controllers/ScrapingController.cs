﻿namespace Scraping.Api.Controllers
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text.RegularExpressions;
    using System.Web.Http;

    using DataModels;
    using HtmlAgilityPack;

    public class ScrapingController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage GetPlainHtml(WebSiteModel url)
        {
            var webGet = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();
            var response = new HttpResponseMessage();

            if (ModelState.IsValid)
            {
                try
                {
                    doc = webGet.Load(url.Url);
                }
                catch (ArgumentNullException)
                {
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    response.Content = new StringContent("The argument is null");
                    return response;
                }
                catch (UriFormatException)
                {
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    response.Content = new StringContent("Wrong url format");
                    return response;
                }
                catch (System.Net.WebException)
                {
                    response.StatusCode = System.Net.HttpStatusCode.BadRequest;
                    response.Content = new StringContent("No such website");
                    return response;
                }

                foreach (HtmlNode link in doc.DocumentNode.SelectNodes("//a[@href]"))
                {
                    link.Attributes.Remove("href");
                }

                var formTags = doc.DocumentNode.SelectNodes("//form");
                if (formTags != null)
                {
                    foreach (HtmlNode form in formTags)
                    {
                        form.Remove();
                    }
                }

                string html = doc.DocumentNode.OuterHtml;
                string pattern = @"<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>";

                string result = Regex.Replace(html, pattern, string.Empty);
                response.Content = new StringContent(result);

                response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
                return response;
            }

            return response;
        }
    }
}
