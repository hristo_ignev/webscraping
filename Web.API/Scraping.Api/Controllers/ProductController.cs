﻿namespace Scraping.Api.Controllers
{
    using System.Linq;
    using System.Web.Http;

    using AutoMapper;
    using Data;
    using DataModels;
    using Services.Contracts;

    public class ProductController : ApiController
    {
        private IScrapingData data;
        private IParseData parser;
        
        public ProductController(IScrapingData data, IParseData parser)
        {
            this.data = data;
            this.parser = parser;
        }

        [HttpGet]
        public IHttpActionResult GetAllProducts(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return this.BadRequest(this.ModelState);
            }

            var user = this.data.Users
                .Include(p => p.Products)
                .Where(u => u.Name == id)
                .FirstOrDefault();

            if (user == null)
            {
                return this.NotFound();
            }

            var getProducts = user.Products
                .Select(u => Mapper.Map<ProductResponseModel>(u))
                .ToList();

            return this.Ok(getProducts);
        }

        [HttpPost]
        public IHttpActionResult AddProducts(AllProductsRequestModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }
            
            var user =
                this.data.Users.Include(p => p.Sites)
                .Where(u => u.Name == model.User.Name)
                .FirstOrDefault();

            var siteXpath = user.Sites
                .Where(u => u.Id == model.Product.SiteId)
                .Select(x => x.Xpath).FirstOrDefault();
           
            foreach (var url in model.Product.Urls)
            {
                user.Products.Add(this.parser.GenerateProductFromXpaht(url, siteXpath));
            }

            this.data.Users.SaveChanges();
                        
            return this.Ok("Products added");
        }
    }
}
