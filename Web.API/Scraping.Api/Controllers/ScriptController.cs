﻿namespace Scraping.Api.Controllers
{
    using System.Linq;
    using System.Web.Http;

    using Scraping.Data;
    using Scraping.Services.Contracts;

    public class ScriptController : ApiController
    {
        private IScrapingData data;
        private IScriptCreator scriptCreator;

        public ScriptController(IScrapingData data, IScriptCreator scriptCreator)
        {
            this.data = data;
            this.scriptCreator = scriptCreator;
        }

        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return this.BadRequest(this.ModelState);
            }

            var user = this.data.Users
                .Include(p => p.Products)
                .Where(u => u.Name == id)
                .FirstOrDefault();

            if (user == null)
            {
                return this.NotFound();
            }

            var allProducts = user.Products;

            string result = this.scriptCreator.CreateString(allProducts);

            return this.Ok(result);
        }
    }
}
