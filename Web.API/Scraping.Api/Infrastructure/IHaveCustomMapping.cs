﻿namespace Scraping.Api.Infrastructure
{
    using AutoMapper;

    public interface IHaveCustomMapping
    {     
        void CreateMapping(IConfiguration config);
    }
}
