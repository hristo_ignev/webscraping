﻿namespace Scraping.Api
{
    using System.Reflection;
    using System.Web.Http;
    using App_Start;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            DataBaseConfig.Initialize();
            MapperConfig.RegisterMappings(Assembly.Load("Scraping.Api"));
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
