﻿namespace Scraping.Api
{
    using System.Data.Entity;
    using Scraping.Data;
    using Scraping.Data.Migrations;

    public static class DataBaseConfig
    {
        public static void Initialize() 
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ScrapingDbContext, Configuration>());
            ScrapingDbContext.Create().Database.Initialize(true);
        }
    }
}