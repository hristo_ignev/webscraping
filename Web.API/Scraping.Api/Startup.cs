﻿using Microsoft.Owin;

[assembly: OwinStartup(typeof(Scraping.Api.Startup))]

namespace Scraping.Api
{
    using System.Reflection;
    using System.Web.Http;

    using Data;
    using Ninject;
    using Ninject.Web.Common.OwinHost;
    using Ninject.Web.WebApi.OwinHost;
    using Owin;
    using Services;
    using Services.Contracts;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(GlobalConfiguration.Configuration);
        }

        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            BindTypes(kernel);

            return kernel;
        }

        private static void BindTypes(StandardKernel kernel)
        {
            kernel.Bind<IScrapingData>().To<ScrapingData>().WithConstructorArgument("dbContext", c => new ScrapingDbContext());
            kernel.Bind<IParseData>().To<ParseData>();
            kernel.Bind<IScriptCreator>().To<ScriptCreator>();
        }
    }
}
