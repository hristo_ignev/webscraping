﻿namespace Scraping.Services
{
    using System;
    using System.Text;

    using Contracts;
    using HtmlAgilityPack;
    using Model;

    public class ParseData : IParseData
    {
        public Product GenerateProductFromXpaht(string url, Xpath xpaths)
        {
            var webGet = new HtmlWeb();
            string salePriceXpath;
            string salePrice = string.Empty;

            HtmlDocument doc = webGet.Load(url);

            if (xpaths.SalePriceXpath != null)
            {
                salePriceXpath = this.FixXpath(doc, xpaths.SalePriceXpath);
                salePrice = this.FixThePrice(doc.DocumentNode.SelectSingleNode(salePriceXpath).InnerHtml.Trim());
            }

            string nameXpath = this.FixXpath(doc, xpaths.NameXpath);
            string descriptionXpath = this.FixXpath(doc, xpaths.DescriptionXpath);
            string shortDescriptionXpath = this.FixXpath(doc, xpaths.ShortDescriptionXpath);
            string regularPriceXpath = this.FixXpath(doc, xpaths.RegularPriceXpath);

            string name = doc.DocumentNode.SelectSingleNode(nameXpath).InnerText.Trim();
            string description = doc.DocumentNode.SelectSingleNode(descriptionXpath).InnerText.Trim();
            string shortDescriptionSecondLine = doc.DocumentNode.SelectSingleNode(shortDescriptionXpath).InnerText.Trim();
            string regularPrice = this.FixThePrice(doc.DocumentNode.SelectSingleNode(regularPriceXpath).InnerText.Trim());
            string productAtributes = "a:0:{}";

            return new Product
            {
                Name = name,
                Description = description,
                ShortDescription = shortDescriptionSecondLine,
                RegularPrice = decimal.Parse(regularPrice),
                SalePrice = string.IsNullOrEmpty(salePrice) ? (decimal?)null : decimal.Parse(salePrice),
                ProductAttributes = productAtributes
            };
        }

        private string FixThePrice(string v)
        {
            if (v == null)
            {
                return null;
            }

            StringBuilder result = new StringBuilder();

            foreach (var character in v)
            {
                bool firstDotFlag = false;
                if (char.IsDigit(character) ||
                    character == '.' ||
                    character == ',')
                {
                    if (character == '.')
                    {
                        firstDotFlag = true;
                        if (firstDotFlag)
                        {
                            continue;
                        }

                        result.Append(character);
                    }

                    result.Append(character);
                }
            }

            result.Replace(',', '.');

            return result.ToString();
        }

        private string FixXpath(HtmlDocument doc, string xpath)
        {
            string[] splitIdAndPath = xpath.Split(new[] { "id(", "'", "\"", ")" }, StringSplitOptions.RemoveEmptyEntries);
            string id = splitIdAndPath[0];
            string restOfTheXpath = string.Empty;

            if (splitIdAndPath.Length > 1)
            {
                restOfTheXpath = splitIdAndPath[1].ToLower();
            }

            if (id == "web-site")
            {
                id = @"html[1]/body[1]";
                return string.Format("//{0}{1}", id, restOfTheXpath);
            }

            string elementName = doc.GetElementbyId(id).Name;

            return string.Format("//{0}[@id='{1}']{2}", elementName, id, restOfTheXpath);
        }
    }
}
