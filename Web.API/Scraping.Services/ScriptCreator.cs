﻿namespace Scraping.Services
{
    using System.Collections.Generic;
    using System.Text;
    using Contracts;
    using Model;

    public class ScriptCreator : IScriptCreator
    {
        public string CreateString(ICollection<Product> productList)
        {
            StringBuilder builder = new StringBuilder();

            int idCounter = 1;
            var storedProcedur = @"DELIMITER $$ DROP PROCEDURE IF EXISTS my_lastId$$ CREATE PROCEDURE my_lastId (OUT param1 INT) BEGIN SELECT MAX( id ) INTO param1 FROM wp_posts; END$$ DELIMITER ; ";
            var pattern = @"http://localhost/WP/?post_type=product&#038;p=";

            builder.Append(storedProcedur);
            foreach (var p in productList)
            {
                builder.Append("CALL my_lastId(@id);");
                builder.Append(@"INSERT INTO wp_posts (ID, post_author, post_content, post_excerpt, ping_status, post_type, post_name, guid, post_title) VALUES (" + "@id +" + (idCounter) + ", '1', '" + p.Description + "', '" + p.ShortDescription + "', 'closed', 'product', '" + this.ConvertBulToEng(p.Name) + "', '" + pattern + "@id" + "', '" + p.Name + "');");

                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key) VALUES(" + "@id" + "+" + idCounter + ",'_edit_lock');");

                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_edit_last','1');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_visibility','visible');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_stock_status','instock');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_wp_old_slug','" + this.ConvertBulToEng(p.Name) + "');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'total_sales','0');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_downloadable','no');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_virtual','no');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_regular_price','" + p.RegularPrice + "');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_sale_price','" + p.SalePrice + "');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_purchase_note','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_featured','no');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_weight','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_length','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_width','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_height','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_sku','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_product_attributes','" + p.ProductAttributes + "');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_sale_price_dates_from','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_sale_price_dates_to','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_price','" + p.SalePrice + "');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_sold_individually','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_manage_stock','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_backorders','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_stock','');");
                builder.Append("INSERT INTO wp_postmeta (post_id,meta_key,meta_value) VALUES(" + "@id +" + idCounter + ",'_product_image_gallery','');");
            }

            return builder.ToString();
        }

        private string ConvertBulToEng(string str)
        {
            string[] engUpper = { "A", "B", "V", "G", "D", "E", "Zh", "Z", "I", "j", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "h", "Ts", "Ch", "Sh", "Sht", "y", "w", "q" };
            string[] engLower = { "a", "b", "v", "g", "d", "e", "zh", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "Sht", "y", "w", "q" };
            string[] bulUpper = { "А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ю", "Я" };
            string[] bulLower = { "а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ю", "я" };
            for (int i = 0; i <= 28; i++)
            {
                str = str.Replace(bulUpper[i], engUpper[i]);
                str = str.Replace(bulLower[i], engLower[i]);
            }

            str = str.Replace(" ", string.Empty);
            return str;
        }
    }
}
