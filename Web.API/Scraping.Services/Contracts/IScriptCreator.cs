﻿namespace Scraping.Services.Contracts
{
    using System.Collections.Generic;

    using Model;

    public interface IScriptCreator
    {
        string CreateString(ICollection<Product> productList);
    }
}
