﻿namespace Scraping.Services.Contracts
{
    using Model;

    public interface IParseData
    {
        Product GenerateProductFromXpaht(string url, Xpath xpaths);
    }
}
