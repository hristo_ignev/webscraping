﻿namespace Scraping.Extensions
{
    using System.Text;

    public static class StringExtensions
    {
        public static string EscapeId(this string id)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < id.Length; i++)
            {
                if (id[i] == '_' ||
                    id[i] == '.' ||
                    id[i] == ',' ||
                    id[i] == '[' ||
                    id[i] == ']')
                {
                    char underscore = '\u0095';
                    result.Append(underscore);
                }
                else
                {
                    result.Append(id[i]);
                }
            }

            return result.ToString();
        }
    }
}
