﻿namespace Scraping.Model
{
    public class Site
    {
        public int Id { get; set; }

        public string Url { get; set; }

        public int UserId { get; set; }
        
        public virtual Xpath Xpath { get; set; }
    }
}
