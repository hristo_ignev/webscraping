﻿namespace Scraping.Model
{
    public class Product
    {
        private decimal? salePrice;

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ShortDescription { get; set; }

        public decimal RegularPrice { get; set; }

        public decimal? SalePrice
        {
            get
            {
                return this.salePrice ?? this.RegularPrice;
            }

            set
            {
                this.salePrice = value;
            }
        }

        public string ProductAttributes { get; set; }

        public int UserId { get; set; }

        public virtual User User { get; set; }
    }
}
