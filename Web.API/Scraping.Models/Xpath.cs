﻿namespace Scraping.Model
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Xpath
    {
        [Key, ForeignKey("Site")]
        public int SiteId { get; set; }

        public string NameXpath { get; set; }

        public string DescriptionXpath { get; set; }

        public string ShortDescriptionXpath { get; set; }

        public string RegularPriceXpath { get; set; }

        public string SalePriceXpath { get; set; }

        public virtual Site Site { get; set; }
    }
}
