﻿namespace Scraping.Model
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        private ICollection<Product> products;
        private ICollection<Site> sites;

        public User()
        {
            this.products = new HashSet<Product>();
            this.sites = new HashSet<Site>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public virtual ICollection<Product> Products
        {
            get
            {
                return this.products;
            }

            set
            {
                this.products = value;
            }
        }

        public virtual ICollection<Site> Sites
        {
            get
            {
                return this.sites;
            }

            set
            {
                this.sites = value;
            }
        }
    }
}
