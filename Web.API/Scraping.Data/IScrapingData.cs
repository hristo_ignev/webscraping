﻿namespace Scraping.Data
{
    using Scraping.Data.Repositories;
    using Scraping.Model;

    public interface IScrapingData
    {
        IGenericRepository<Xpath> Xpaths { get; }

        IGenericRepository<Site> Sites { get; }

        IGenericRepository<User> Users { get; }

        IGenericRepository<Product> Products { get; }
    }
}
