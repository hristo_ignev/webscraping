﻿namespace Scraping.Data
{
    using System.Data.Entity;

    using Model;

    public class ScrapingDbContext : DbContext, IScrapingDbContext
    {
        public ScrapingDbContext()
            : base("ScrapingDb")
        {
        }

        public IDbSet<User> Users { get; set; }

        public IDbSet<Product> Products { get; set; }

        public IDbSet<Site> Sites { get; set; }

        public IDbSet<Xpath> Xpaths { get; set; }

        public static ScrapingDbContext Create()
        {
            return new ScrapingDbContext();
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        IDbSet<TEntity> IScrapingDbContext.Set<TEntity>()
        {
            return base.Set<TEntity>();
        }
    }
}
