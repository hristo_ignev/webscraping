﻿namespace Scraping.Data
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using Model;

    public interface IScrapingDbContext
    {
         IDbSet<User> Users { get; set; }

         IDbSet<Product> Products { get; set; }

         IDbSet<Site> Sites { get; set; }

         IDbSet<Xpath> Xpaths { get; set; }
       
         IDbSet<TEntity> Set<TEntity>() where TEntity : class;

         DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

         int SaveChanges();
    }
}
