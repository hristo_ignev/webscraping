namespace Scraping.Data.Migrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<Scraping.Data.ScrapingDbContext>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
            this.ContextKey = "Scraping.Data.ScrapingDbContext";
        }

        protected override void Seed(ScrapingDbContext context)
        {
        }
    }
}
