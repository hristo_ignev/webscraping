﻿namespace Scraping.Data.Repositories
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IGenericRepository<T>
    {
        IQueryable<T> All();

        IQueryable<T> Search(Expression<Func<T, bool>> conditions);

        IQueryable<T> Include<TProparty>(Expression<Func<T, TProparty>> conditions);

        void Add(T entity);

        T Find(Expression<Func<T, bool>> conditions);

        T Delete(T entity);

        void Update(T entity);

        void Detach(T entity);

        int SaveChanges();
    }
}
