﻿namespace Scraping.Data.Repositories
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;

    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private IScrapingDbContext context;

        public GenericRepository(IScrapingDbContext scrapingDbContext)
        {
            this.context = scrapingDbContext;
        }

        public void Add(T entity)
        {
            this.ChangeState(entity, EntityState.Added);
        }

        public IQueryable<T> All()
        {
            return this.context.Set<T>();
        }

        public T Delete(T entity)
        {
            this.ChangeState(entity, EntityState.Deleted);
            return entity;
        }

        public void Detach(T entity)
        {
            this.ChangeState(entity, EntityState.Detached);
        }

        public T Find(Expression<Func<T, bool>> conditions)
        {
            return this.All().FirstOrDefault(conditions);
        }

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }

        public IQueryable<T> Search(Expression<Func<T, bool>> conditions)
        {
            return this.All().Where(conditions);
        }

        public IQueryable<T> Include<TProparty>(Expression<Func<T, TProparty>> conditions)
        {
            return this.All().Include(conditions);
        }

        public void Update(T entity)
        {
            this.ChangeState(entity, EntityState.Modified);
        }

        private void ChangeState(T entity, EntityState state)
        {
            this.context.Set<T>().Attach(entity);
            this.context.Entry(entity).State = state;
        }
    }
}
