﻿namespace Scraping.Data
{
    using System;
    using System.Collections.Generic;

    using Model;
    using Repositories;

    public class ScrapingData : IScrapingData
    {
        private IScrapingDbContext context;
        private IDictionary<Type, object> repositories;

        public ScrapingData(IScrapingDbContext dbContext)
        {
            this.context = dbContext;
            this.repositories = new Dictionary<Type, object>();
        }

        public IGenericRepository<Product> Products
        {
            get
            {
                return this.GetRepository<Product>();
            }
        }

        public IGenericRepository<Site> Sites
        {
            get
            {
                return this.GetRepository<Site>();
            }
        }

        public IGenericRepository<User> Users
        {
            get
            {
                return this.GetRepository<User>();
            }
        }

        public IGenericRepository<Xpath> Xpaths
        {
            get
            {
                return this.GetRepository<Xpath>();
            }
        }

        private IGenericRepository<T> GetRepository<T>() where T : class
        {
            var typeOfModel = typeof(T);
            if (!this.repositories.ContainsKey(typeOfModel))
            {
                var type = typeof(GenericRepository<T>);
                this.repositories.Add(typeOfModel, Activator.CreateInstance(type, this.context));
            }

            return (IGenericRepository<T>)this.repositories[typeOfModel];
        }
    }
}
