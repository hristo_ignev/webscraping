'use strict';

module.exports = {
    get: function (req) {

        var user = {};
        if (req.isAuthenticated()) {
            user.isAuth = true;
            user.name = req.user.local.email;
        }

        var message = req.flash('message');

        return {
            user: user,
            title: 'Web Scraping',
            message: message
        }
    }
};

