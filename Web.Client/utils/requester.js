'use strict';

var http = require('http');
var querystring = require('querystring');
var Promise = require('bluebird');

function send(method, url, options) {
    var dataBuffer = '';
    var data = options.data || undefined;
    var postData = options.json ? JSON.stringify(data) : querystring.stringify(data);
    var headers =  {
            'Content-Type':   options.json ? 'application/json' : 'application/x-www-form-urlencoded',
            'Content-Length': postData.length
        };

    console.log(postData);

    var promise = new Promise(function (resolve, reject) {
        var options = {
            host: 'localhost',
            port: 62753,
            path: url,
            method: method,
            headers: headers
        };

        var req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                dataBuffer += chunk;
            });

            res.on('end', function () {
                resolve(dataBuffer);
            });
        });

        req.on('error', function (err) {
            reject(err);
        });

        req.write(postData);
        req.end();
    });

    return promise;
}

module.exports = {
    get: function (url, options) {
        return send('GET', url, options);
    },
    post: function (url, options) {
        return send('POST', url, options);
    },

    put: function (url, options) {
        return send('PUT', url, options);
    }
};
