(function () {

    //get site
    $('#btn-url').on('click', function () {
        var url = $('#tb-url').val();
        var regex = /^(?:https?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)/igm;
        $('#url').val((regex.exec(url))[0]);

        var options = {
            data: {
                url: url
            }
        };
        send('POST', '/previewHtml', options)
            // send('GET', 'test.html')
            .then(function (resHtml) {
                var html = resHtml.result;
                // var html = resHtml;
                $('#web-site').html(html);
            })
            .then(function(){
                $('base').remove();
            })
    });//end get site

    // color
    var color;
    var element;
    var body = document.getElementById('web-site');

    body.addEventListener("mouseover", function (event) {
        if (element != null) {
            element.style.backgroundColor = color
        }
        element = event.target;
        color = event.target.style.backgroundColor;
        event.target.style.backgroundColor = "rgba(29, 145, 184, 0.7)";
    }, false);
    // color

    // !!!MAGIC
    var currentInputId;

    $('#product-container').on('click', 'input', function () {
        var $this = $(this),
            $hiddenInput = $this.next('input');

        var body = document.getElementById('web-site');
        currentInputId = $this.attr('id');

        body.addEventListener('click', function (event) {
            if ($this.attr('id') !== currentInputId) {
                return
            }

            if (event === undefined) event = window.event;
            {
                var target = 'target' in event ? event.target : event.srcElement; // another IE hack
            }

            var path = getPathTo(target);

            $this.val($(target).text());
            $hiddenInput.val(path);
        });

        function getPathTo(element) {
            if (element.id !== '') {
                return 'id("' + element.id + '")';
            }

            if (element === document.body) {
                return element.tagName;
            }

            var ix = 0;
            var siblings = element.parentNode.childNodes;
            for (var i = 0; i < siblings.length; i++) {
                var sibling = siblings[i];
                if (sibling === element) {
                    return getPathTo(element.parentNode) + '/' + element.tagName + '[' + (ix + 1) + ']';
                }

                if (sibling.nodeType === 1 && sibling.tagName === element.tagName) {
                    ix++;
                }
            }
        }
    }); //end !!!MAGIC!!!

    function send(method, url, options) {
        options = options || {};

        var headers = options.headers || {},
            data = options.data || undefined,
            contentType = options.contentType || 'application/json';

        var promise = new Promise(function (resolve, reject) {
            $.ajax({
                url: url,
                method: method,
                contentType: contentType,
                headers: headers,
                data: JSON.stringify(data),
                success: function (res) {
                    resolve(res);
                },
                error: function (err) {
                    reject(err);
                }
            });
        });
        return promise;
    }
}());
