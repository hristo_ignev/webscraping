window.getPath = function ($elementName, $elementXpath) {


  //  var body =  document.getElementById('web-site');
    var body =  document.getElementById('header-alabala');
    //Get Xpath
    //Event must be attached on scraped part of the div, but for testing in airplane will be attached on documentBody
    body.addEventListener('click', function (event) {
        if (event === undefined) event = window.event;                     // IE hack
        var target = 'target' in event ? event.target : event.srcElement; // another IE hack
        var path = getPathTo(target);
        $elementName.val($(target).text());
        $elementXpath.val(path);
        console.log(path);
        console.log($(target).text())
        console.log($elementXpath.val());
    });

    function getPathTo(element) {
        if (element.id !== '')
            return 'id("' + element.id + '")';
        if (element === document.body)
            return element.tagName;

        var ix = 0;
        var siblings = element.parentNode.childNodes;
        for (var i = 0; i < siblings.length; i++) {
            var sibling = siblings[i];
            if (sibling === element)
                return getPathTo(element.parentNode) + '/' + element.tagName + '[' + (ix + 1) + ']';
            if (sibling.nodeType === 1 && sibling.tagName === element.tagName)
                ix++;
        }
    }
};
