(function(){$('#form').on('click', '.btn-add', function () {
    var $list = $('#list');
    var $elem = $('.clone').first();

    var $clone = $elem.clone();

    $clone.find(escapeId('Urls[0]'))
        .val('');

    console.log($clone.find(escapeId('Urls[0]')));

    $clone.find('i').attr('class', '')
        .attr('class', 'input-group-addon glyphicon glyphicon-remove-sign btn-remove');
    $list.append($clone);
    fixIds();
});

$('#form').on('click', '.btn-remove', function () {
    $(this).closest('.clone').remove();
    fixIds();
});

function fixIds() {
    var $clones = $('.clone');

    $clones.each(function (index) {
        var $this = $(this);

        $this
            .find('input')
            .attr('name', 'Urls[' + index + ']')
            .attr('id', 'urls[' + index + ']');


        var $button = $this.find('i.fa');
        $button.attr('class', '');

        if (index === 0) {
            $button.addClass('input-group-addon glyphicon glyphicon-plus btn-add');
        } else {
            $button.addClass('input-group-addon glyphicon glyphicon-remove-sign btn-remove');
        }
    })
}

function escapeId(id) {
    return "#" + id.replace(/(:|\.|\[|\]|,)/g, "\\$1");
}
})();