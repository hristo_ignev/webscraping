'use strict';

var express = require('express');
var passport = require('passport');

var app = express();
var env = process.env.NODE_ENV || 'development';
var envConfig = require('./config/envConfig')[env];


require('./config/mongooseConf')(envConfig);

require('./config/passportConf').initialize(passport);

require('./config/expressConf')(app, passport);

require('./config/routesConf')(app, passport);

app.listen(envConfig.port, function () {
  console.log('server is running at port ' + envConfig.port);
});
