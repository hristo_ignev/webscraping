'use strict';

var homeController = require('./homeController');
var previewHtml = require('./previewHtml');
var userController = require('./userController');
var addSiteController = require('./addSiteController');
var mySitesController = require('./mySitesController');
var addProductsController = require('./addProductsController');
var myProductsController = require('./myProductsController');
var myWComersScriptControler = require('./myWComersScriptControler');

module.exports = {
    home: homeController,
    previewHtml: previewHtml,
    user: userController,
    addSite: addSiteController,
    mySites: mySitesController,
    addProducts: addProductsController,
    myProducts: myProductsController,
    myWComersScript: myWComersScriptControler
};
