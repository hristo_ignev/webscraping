'use strict';

var requester = require('../utils/requester');
var commonViewModel = require('../utils/commonViewModel');

module.exports = {
    init: function (req, res) {
        var viewModel = commonViewModel.get(req);
        res.render('addSite', viewModel);
    },
    addSite: function(req, res){
        var data ={};
        data.product = {
            NameXpath :req.body.NameXpath,
            Url :req.body.Url,
            DescriptionXpath :req.body.DescriptionXpath,
            ShortDescriptionXpath :req.body.ShortDescriptionXpath,
            RegularPriceXpath :req.body.RegularPriceXpath,
            SalePriceXpath :req.body.SalePriceXpath
        };
        data.user = {
            name: req.user.local.email
        };

        var url = '/api/website';
        var options = {};

        options.data = data;
        options.json = true;
        requester.post(url, options)
            .then(function (resData) {
                req.flash('message', resData);
                res.redirect('/addSite')
            })
            .catch(function (err) {
                req.flash('message', resData);
                res.redirect('/addSite')
            });
    }
};


