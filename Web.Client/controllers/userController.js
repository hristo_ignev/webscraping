'use strict';

var path = require('path');

module.exports = {
    signup: function (req, res) {
        var message = req.flash('signupMessage');
        console.log(message);
        res.render('signup', {message: message});
    },
    login: function(req, res){
        var message = req.flash('loginMessage');
        console.log(message);
        res.render('login', {message: message});
    },
    logout: function(req, res){
        req.logout();
        res.redirect('/');
    }
};
