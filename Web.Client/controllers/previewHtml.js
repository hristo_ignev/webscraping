        'use strict';

var requester = require('../utils/requester');

module.exports = {
    init: function (req, res) {
        var data = req.body;
        var url = '/api/Scraping';
        var options = {};

        options.data = data;

        requester.post(url, options)
            .then(function (resData) {
                res.send({success: true, result: resData})
            })
            .catch(function (err) {
                res.json({success: false, reason: err});
            });
    }
};
