'use strict';

var commonViewModel = require('../utils/commonViewModel');
var requester = require('../utils/requester');

module.exports = {
    init: function (req, res) {
        var url = '/api/script?id=' +  req.user.local.email ;
        var options = {};

        options.data = {
            id: req.user.local.email
        };

        var viewModel = commonViewModel.get(req);

        requester.get(url, options)
            .then(function (resData) {
                console.log(resData);
                viewModel.script = JSON.parse(resData);
                console.log(resData);
                res.render('myWComersScript', viewModel);
            })
            .catch(function (err) {
                viewModel.message = ['No products found!'];
                res.render('myWComersScript', viewModel)
            });
    }
};