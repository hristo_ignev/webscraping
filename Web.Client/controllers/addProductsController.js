'use strict';

var requester = require('../utils/requester');
var commonViewModel = require('../utils/commonViewModel');

module.exports = {
    init: function (req, res) {
        var viewModel = commonViewModel.get(req);

        viewModel.site = {
            id: req.query.id
        };

        res.render('addProducts', viewModel);
    },
    addProducts: function(req, res){
        var data ={};
        data.product = req.body;
        data.user = {
            name: req.user.local.email
        };

        console.log(data);
        var url = '/api/product';
        var options = {};

        options.data = data;
        options.json = true;
        requester.post(url, options)
            .then(function (resData) {
                req.flash('message', resData);
                res.redirect('/addProducts')
            })
            .catch(function (err) {
                req.flash('message', resData);
                res.redirect('/addProducts')
            });
    }
};
