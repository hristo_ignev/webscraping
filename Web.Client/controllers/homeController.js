'use strict';

var commonViewModel = require('../utils/commonViewModel');

module.exports = {
    init: function (req, res) {
        var viewModel = commonViewModel.get(req);
        res.render('index', viewModel);
    }
};
