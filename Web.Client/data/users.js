'use strict';

var User = require('mongoose').model('User');
var Promise = require('bluebird');

module.exports = {
    create: function (email, password) {
        var promise = new Promise(function (resolve, reject) {
            User.findOne({'local.email': email}, function (err, user) {
                if (err) {
                    return reject(err)
                }

                if (user) {
                    return reject('That email is already taken.');
                } else {
                    var newUser = new User();
                    newUser.local.email = email;
                    newUser.local.password = newUser.generateHash(password);

                    newUser.save(function (err) {
                        if (err) {
                            throw err;
                        }
                        resolve(null, newUser);
                    });
                }
            });
        });

        return promise;
    },
    findById: function (id, callback) {
            User.findById(id, callback)
    },
    findByEmail: function (email, password) {
        var promise = new Promise(function (resolve, reject) {
            User.findOne({'local.email': email}).exec(function (err, dbUser) {
                if (err) {
                    return reject(err);
                }

                if (!dbUser) {
                    return reject('User not found.');
                }

                if (!dbUser.validPassword(password)) {
                    return reject('Oops! Wrong password.');
                }

                resolve(dbUser);
            });
        });

        return promise;
    }
};
