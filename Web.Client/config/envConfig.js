'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname +'./..');

module.exports = {
  development:{
    rootPath : rootPath,
    port: 3030,
    dbConnection: 'mongodb://pesho:pesho@ds045694.mongolab.com:45694/webscraping'
  },
  production:{
    rootPath : rootPath,
    port: process.env.Port ||3030,
    dbConnection: 'mongodb://pesho:pesho@ds045694.mongolab.com:45694/webscraping'
  }
};
