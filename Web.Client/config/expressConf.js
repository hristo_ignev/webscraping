'use strict';

var express = require('express');
var bodyParser = require('body-parser');
var path = require('path');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var flash = require('connect-flash');
var handlebars  = require('express-handlebars');
var handlebarsConfig = require ('./handlebarsConf');
var sessionConfig = require('./sessionConf');

module.exports = function (app, passport) {

    app.set('views', path.resolve(__dirname + '/../views'));

    app.engine('handlebars', handlebars(handlebarsConfig));

    app.set('view engine', 'handlebars');

    app.use(morgan('dev'));

    app.use(cookieParser());

    app.use(bodyParser.urlencoded({extended: true}));

    app.use(bodyParser.json());

    app.use(session(sessionConfig));

    app.use(passport.initialize());

    app.use(passport.session());

    app.use(flash());

    app.use(express.static('public'));
};
