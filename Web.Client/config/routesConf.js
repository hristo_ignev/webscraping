'use strict';

var controllers = require('../controllers');
var passportConfig = require('./passportConf');
var customMiddleman  = require('../middlemen/customMiddleman');

module.exports = function (app, passport) {
    app.get('/', controllers.home.init);

    app.get('/login', controllers.user.login);

    app.get('/signup', controllers.user.signup);

    app.get('/logout', controllers.user.logout);

    app.get('/addSite', customMiddleman.isAuthenticated, controllers.addSite.init);

    app.post('/addSite', customMiddleman.isAuthenticated, controllers.addSite.addSite);

    app.get('/mySites', customMiddleman.isAuthenticated, controllers.mySites.init);

    app.get('/myWComersScript', customMiddleman.isAuthenticated, controllers.myWComersScript.init);

    app.get('/addProducts:id*?', customMiddleman.isAuthenticated, controllers.addProducts.init);

    app.post('/addProducts', customMiddleman.isAuthenticated, controllers.addProducts.addProducts);

    app.get('/myProducts', customMiddleman.isAuthenticated, controllers.myProducts.init);

    app.post('/previewHtml', customMiddleman.isAuthenticated, controllers.previewHtml.init);

    app.post('/signup', passport.authenticate('local-signup', passportConfig.localSignUpStrategy));

    app.post('/login', passport.authenticate('local-login', passportConfig.localLogInpStrategy));
};


