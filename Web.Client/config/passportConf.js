'use strict';

var LocalStrategy = require('passport-local').Strategy;
var users = require('../data/users');

module.exports = {
    initialize: function (passport) {

        passport.serializeUser(function (user, done) {
            done(null, user.id);
        });

        passport.deserializeUser(function (id, done) {
            users.findById(id, function (err, user) {
                done(err, user);
            });
        });

        passport.use('local-signup', new LocalStrategy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, email, password, done) {
                process.nextTick(function () {
                    users.create(email, password)
                        .then(function (user) {
                            return done(null, user);
                        })
                        .catch(function (err) {
                            return done(null, false, req.flash('signupMessage', err));
                        });
                });
            }));

        passport.use('local-login', new LocalStrategy({
                usernameField: 'email',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, email, password, done) {
                users.findByEmail(email, password)
                    .then(function (user) {
                        return done(null, user);
                    })
                    .catch(function (err) {
                        console.log(err);
                        return done(null, false, req.flash('loginMessage', err))
                    });
            }));
    },
    localSignUpStrategy: {
        successRedirect: '/login',
        failureRedirect: '/signup',
        failureFlash: true
    },
    localLogInpStrategy :  {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }
};
